# myproject



## TP

this 'TP' is meant to train and to familiarize yourself with git and its commands as well as gitlab/github.
the work requested is :

### 1. Create New Directory
create an empty folder/directory.
### 2. Navigate to Working Directory
open terminal and change working directory to the one you just created.
### 3. Clone
clone this repository using the command : 
``` 
git clone <repo-url>

```
change < repo-url > with the HTTPS value u get when you click on the clone drop-down (blue button) located in the top.
### 4. Make Changes
open vs code and create a new txt file named after you, like : fullname.txt.
### 5. Stage Changes
stage your changes using the command : 
```
git add .
```
### 6. Commit
commit your changes using : 
```
git commit -m 'commit message'
```
### 7. Add Access Token
this step is required since you have no access to 'writing to repository'.
First you need to copy the access token provided by the project owner.
the your going to set the remote repo link to use it via the command : 
```
git remote set-url origin https://<your-token>@gitlab.com/your-username/your-repo.git

```
so it should look like
```
git remote set-url origin https://<your-token>@gitlab.com/omarKhalil/myproject.git
```
you still need to change < your-token > with the access token provided.

### 8. Push
push your local changes to the remote repo using :
```
git push origin main
```
When running this command you are going to be asked to enter a password, do not enter your account password, instead enter the access token provided bty the project owner

